from django.shortcuts import render, HttpResponse
from app_status.models import Status
from app_profile.models import Profile
from app_profile.views import mhs_name
from app_friend.models import Friend

# Create your views here.
response = {}

def index(request):
    totalFeed = Status.objects.all().count()
    friends = Friend.objects.all().count()
    profiles = Profile.objects.all().count()

    response['total_feed'] = totalFeed
    response['total_friend'] = friends
    response['latest_post'] = '-'
    response['latest_post_date'] = '-'
    response['name'] = mhs_name

    if(totalFeed != 0):
        s = Status.objects.last()
        response['latest_post'] = s.message
        response['latest_post_date'] = s.created_date

    return render(request, 'app_dashboard.html', response)
