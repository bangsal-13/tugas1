from django.shortcuts import render
from datetime import date
from .models import Profile, Expertise
# Create your views here.


mhs_name = 'Aviliani Pramestya'
birth_date = date(1997, 12, 4)
mhs_gender = 'Female'
mhs_expertise = ['Python', 'Java', 'Django']
mhs_description = 'We are just what we are'
mhs_email = 'kelompok13@gmail.com'

def index(request):
    profile = Profile(name=mhs_name,gender=mhs_gender, description=mhs_description, email=mhs_email)
    expertise = Expertise(expertise=mhs_expertise)
    response = {'Name': profile.name,'Birthday': birth_date,'Gender':profile.gender, 'Description':profile.description, 'Email': profile.email, 'Expert' : expertise.expertise}
    return render(request, 'app_profile.html', response)
